from flask import Blueprint, request

from lib import public_endpoint
from lib.gitlab import gitlab_get_webhook_token

hooks = Blueprint('hooks', __name__)


# TODO trigger project build
@public_endpoint
@hooks.route('/hook', methods=["POST", "GET"])
def hook():
    token = request.headers.get('X-Gitlab-Token')
    if not (token == gitlab_get_webhook_token()):
        return dict(ok=False), 401
    return dict(ok=True)
