from flask import Blueprint, render_template, request, redirect

from lib.gitlab import gitlab_set_personal_token, gitlab_get_personal_token, gitlab_set_webhook_token

settings = Blueprint('settings', __name__)


@settings.route('/settings', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        gitlab_personal_token = request.form.get('gitlab_personal_token')
        if gitlab_personal_token is not None:
            gitlab_set_personal_token(gitlab_personal_token)

        gitlab_webhook_token = request.form.get('gitlab_webhook_token')
        if gitlab_webhook_token is not None:
            gitlab_set_webhook_token(gitlab_webhook_token)

        return redirect('/')

    return render_template('settings.html', token=gitlab_get_personal_token())
