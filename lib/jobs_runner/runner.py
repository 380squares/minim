import json
import threading
from time import sleep

from db.db import row_to_dict
from db.jobs import create_job, update_job_step, update_job_status, get_job_by_uuid
from lib.gitlab import gitlab_download_artifact
from lib.jobs import KIND_ARTIFACT, KIND_DOCKER_BUILD, KIND_SHELL, DONE, FAIL
from lib.jobs_runner.shell_job import shell_job
from lib.socket import set_is_running
from .download_artifact_job import download_artifact_job

"""
Extract all artifacts form all steps 
"""


def get_job_all_artifacts(job):
    steps_wit_infos = json.loads(job['steps'])
    artifacts = []
    print("get_job_all_artifacts")
    for item in steps_wit_infos:
        if 'info' in item:
            if 'artifact' in item['info']:
                artifacts.append(item['info']['artifact'])
    return artifacts


def run(project):
    print(">>>>>>>> Starting new project")
    try:
        pipeline_steps = json.loads(project['pipeline_steps'])
        pipeline_env = json.loads(project['pipeline_envs'])
        build_branch = project['build_branch']
        id_project = project['id']
        job = create_job(id_project, build_branch)
        job_uuid = job['uuid']
        print("job >>>>>>>>>>>>>>>>")
        update_job_status(job_uuid, 'RUNNING')
        job = get_job_by_uuid(job_uuid)
        set_is_running(True, json.dumps(row_to_dict(job)))
        for step in pipeline_steps:
            job = get_job_by_uuid(job_uuid)
            job_type = step['type']
            if job_type == KIND_ARTIFACT:
                info = json.loads(step['value'])
                if info:
                    # get variables of args
                    gitlab_job = info['job']
                    artifact_name = info['output']
                    if gitlab_job is not None:
                        print(" gitlab_job >>>> %s" % gitlab_job)
                        print("gitlab_download_artifact in job %s for branch %s on project %s" % (
                            job, build_branch, str(id_project)))
                        status, artifact_path = download_artifact_job(job, KIND_ARTIFACT,
                                                                      gitlab_download_artifact(id_project, build_branch,
                                                                                               gitlab_job))
                        update_job_step(job['uuid'], step,
                                        dict(status=status, artifact=dict(name=artifact_name, path=artifact_path)))
            if job_type == KIND_SHELL:
                status, details = shell_job(step, get_job_all_artifacts(job))
                update_job_step(job['uuid'], step,
                                dict(status=status, details=details))

        # if (step['type']) == KIND_DOCKER_BUILD:
        # think about git here
        # build_docker_image(path.join(os.getcwd(), 'Dockerfile'), pipeline_env)
        set_is_running(False, None)
        update_job_status(job_uuid, DONE)
    except Exception as e:
        print("Exception #####")
        print(e)
        update_job_status(job['uuid'], FAIL)
        set_is_running(False, None)


class JobsRunner(object):
    def __init__(self, interval=1):
        self.interval = interval

    def run(self, project):
        print('Running on >>> ' + str(project['id']))
        thread = threading.Thread(target=run, args=(project,))
        thread.daemon = True
        thread.start()
