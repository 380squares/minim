"""
    job
    step
    step=artifact_name, path=artifact_path

    example : unzip ${build_assets}
"""
import re


def shell_job(job, artifacts):
    command = job['value']
    print(artifacts)
    for artifact in artifacts:
        if 'name' in artifact and 'path' in artifact:
            command = re.sub(r"\${" + artifact['name'] + "}", artifact['path'], command)

    print(command)
    return "done", command
