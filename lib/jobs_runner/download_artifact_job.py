import json
from os import path
from uuid import uuid1

from lib.helpers import get_tmp_folder
from lib.jobs import FAIL, DONE
from lib.socket import send_io_message


def download_artifact_job(job, step, request):
    print('start downloading artifact >>>>> ')
    output_name = path.join(get_tmp_folder(), 'artifact-' + str(uuid1()))
    with request as r:
        if r.status_code == 404:
            send_io_message(
                json.dumps({"is_running": False, "job_uuid": job['uuid'], "type": "error", "debug": "download"}))
            return FAIL, r.status_code
        with open(output_name, 'wb') as f:
            total = int(r.headers['Content-Length'])
            progress = 0
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    progress += len(chunk)
                    print(str(progress / total))
                    send_io_message({
                        "is_running": True,
                        "job_uuid": job['uuid'],
                        "step": step,
                        "progress": str(progress / total)
                    })
    return DONE, output_name
