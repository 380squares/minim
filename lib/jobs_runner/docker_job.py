import docker

client = docker.DockerClient()


def build_docker_image(dockerfile, build_envs, name=None):
    kwargs = {'dockerfile': dockerfile, 'build_args': build_envs, 'name': name}
    client.images.build(**kwargs)


def docker_run_image(run_args, ports):
    # name
    # environment
    client.containers.run()
