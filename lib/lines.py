# !/usr/bin/env python3.
"""
Very simple HTTP server in python for logging requests
Usage::
    ./main.py [<port>]
"""

import subprocess
import os
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json

GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")

mapping = dict({
    'lines': '/home/work/lines/deploy.sh',
    'web-app': '/home/work/unk/deploy.sh',

})


def execute(project):
    logging.info("Starting execute >>> ")
    logging.info(mapping[project])
    subprocess.call(['sh', mapping[project]])


class S(BaseHTTPRequestHandler):
    def _reply(self, status=200, text=None):
        self.send_response(status)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write("POST request for {}".format(text).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        gitlab_token = self.headers['X-Gitlab-Token']  # <--- Gets the size of data
        if not gitlab_token or gitlab_token != GITLAB_TOKEN:
            self._reply(500)
        else:
            post_data = self.rfile.read(content_length)  # <--- Gets the data itself

            """
            logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                         str(self.path), str(self.headers), post_data.decode('utf-8'))
             """
            info = json.loads(post_data)
            if info and info['repository'] and info['repository']['name']:
                project = info['repository']['name']
                if mapping[project] is not None:
                    self._reply(200, 'ok')
                    execute(project)

            else:
                self._reply(500)


def run(server_class=HTTPServer, handler_class=S, port=88):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
