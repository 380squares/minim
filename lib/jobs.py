KIND_SHELL = "SHELL"
KIND_ARTIFACT = "ARTIFACT"
KIND_DOWNLOAD = "DOWNLOAD"
KIND_DOCKER_BUILD = "DOCKER_BUILD"
KIND_DOCKER_RUN = "DOCKER_RUN"
KIND_NPM = "NPM"
TYPES = [KIND_SHELL, KIND_ARTIFACT, KIND_DOWNLOAD, KIND_DOCKER_BUILD, KIND_DOCKER_RUN, KIND_NPM]
TYPES_DESCRIPTION = {}
TYPES_DESCRIPTION[KIND_ARTIFACT] = '{"job": "<gitlab_job_ref>", "output" : "<artifact_build_name>"}'
TYPES_DESCRIPTION[KIND_SHELL] = "unzip ${artifact_build_name} -d /opt/target_dir"


def get_required_params():
    if type == KIND_ARTIFACT:
        return ['job', 'output']
    elif type == KIND_SHELL:
        return ['']


PENDING = "PENDING"
DONE = "DONE"
ABORT = "ABORT"
FAIL = "FAILED"
