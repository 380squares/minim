import threading
import time

from flask_socketio import SocketIO

is_running = False
current_job = None


def set_is_running(value, job):
    global is_running
    global current_job
    is_running = value
    current_job = job


socketio = None


def get_socket(app):
    global socketio
    socketio = SocketIO(app, async_mode="eventlet")

    @socketio.on('connect')
    def handle_message():
        socketio.emit("message", "connect")

    return socketio


def send_io_message(message):
    global socketio
    socketio.sleep(0)
    socketio.emit("message", dict(event="status", payload=message))


def check():
    global socketio
    while True:
        print('checking')
        time.sleep(2)
        socketio.emit("message", dict(event="status", payload=dict(job=current_job, is_running=is_running)))


def send():
    global socketio
    while True:
        print('checking')
        time.sleep(2)
        socketio.emit("message", dict(event="status", payload=dict(job=current_job, is_running=is_running)))


d = threading.Thread(name='daemon', target=check)
d.setDaemon(True)
d.start()
