import json
from uuid import uuid4

from db.db import get_cursor, commit_db

CREATE_TABLE_JOBS_SQL = """
CREATE TABLE IF NOT EXISTS jobs (
 uuid GUID PRIMARY KEY,
 project_id int,
 build_branch varchar ,
 status VARCHAR ,
 steps json,
 debug json,
 FOREIGN KEY (project_id) REFERENCES projects (id))"""

INSERT_JOB_SQL = '''INSERT INTO jobs(uuid, project_id, build_branch, status) VALUES(?,?,?,'PENDING') '''


def get_jobs():
    return ""


def create_job(project_id, build_branch):
    # https://docs.gitlab.com/ee/api/projects.html#get-single-project
    uuid = uuid4()
    get_cursor().execute(INSERT_JOB_SQL, (str(uuid), project_id, build_branch))
    commit_db()
    return get_job_by_uuid(uuid)


def get_job_by_uuid(uuid):
    return get_cursor().execute("Select * from jobs where uuid= ? ;", (str(uuid),)).fetchone()


def update_job_step(uuid, step, info, debug={}):
    job = get_job_by_uuid(uuid)
    steps = []
    if 'steps' in job:
        steps = json.loads(job['steps'])
    steps.append(dict(step=step, info=info))
    # steps & env already stringified
    get_cursor().execute("UPDATE jobs set steps = ?, debug = ? where uuid=? ",
                         (json.dumps(steps), json.dumps(steps), str(uuid)))
    commit_db()


def update_job_status(uuid, status):
    # steps & env already stringified
    get_cursor().execute(" UPDATE jobs set status= ?  where uuid=? ",
                         (status, str(uuid)))
    commit_db()
