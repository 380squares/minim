
A minimal ci cd tools to run pipelines inside a host machine (VPS, VM, bare metal)
Used as an internal project 380squares projects.

#### ENV : 

`MINIM_FLASK_APP_SECRET_KEY` flask secret app 

`MINIM_AUTH_KEY` the login authentication key 

`MINIM_GITLAB_TOKEN` Gitlab personal access token (it can be configured via Gitlab)

`MINIM_GITLAB_TOKEN` Gitlab webhook token (it can be configured via UI)

#### Run in docker : 

#todo 

Next : 

[] Move pipeline into separate database

[] Allow multiple pipeline for simple project

Plugins : 
 - Gitlab hooks
 